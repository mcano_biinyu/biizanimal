﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainEvo2 : MonoBehaviour
{
    // Variables de estadisticas


    public bool pressed = false;
    public bool seguir = true;

    public GameObject Moneda;

    float posX;
    float posY;

    private GameObject col;
    private GameObject Animal;

    //private Transform positionObj;

    // Validar que la posición del objecto se capture 
    bool valPos = false;

    //Variables de reposicionamiento
    public bool RePosition = false;
    public bool DentroLimite = false;
    public GameObject targ;

    public float VarChangeAnimation = 0f;

    void Start()
    {
        StartCoroutine(generadorOro());
        StartCoroutine(ChangeAnimation());
        targ = GameObject.Find("Centro");
    }

    // Update is called once per frame
    void Update()
    {

        if (pressed == true)
        {

            this.GetComponent<SpriteRenderer>().sortingOrder = 2;

            //Set the position to the mouse position
            this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y,
                0.0f);
        }

        //Gets the world position of the mouse on the screen        
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Checks whether the mouse is over the sprite
        bool overSprite = this.GetComponent<SpriteRenderer>().bounds.Contains(mousePosition);

        //If it's over the sprite
        if (overSprite)
        {
            //If we've pressed down on the mouse (or touched on the iphone)
        }

        if (Input.GetMouseButtonUp(0) && seguir == false)
        {
            pressed = false;
            this.GetComponent<SpriteRenderer>().sortingOrder = 3;


            GameObject.Find("ControladorEventosEvo1").GetComponent<EventControllerEvo1>().object2 = this.gameObject;
            GameObject.Find("ControladorEventosEvo1").GetComponent<EventControllerEvo1>().object3 = col.gameObject;

            posX = this.transform.position.x + col.transform.position.x;
            posY = this.transform.position.y + col.transform.position.y;

            GameObject.Find("ControladorEventosEvo1").GetComponent<EventControllerEvo1>().ContraladorEvo0(this.gameObject.name, col.gameObject.name);

            seguir = true;


            valPos = true;
        }
        if (Input.GetMouseButtonUp(0) && seguir == true)
        {
            pressed = false;
            this.GetComponent<SpriteRenderer>().sortingOrder = 3;
            valPos = false;
        }

        if (Input.GetMouseButtonUp(0) && DentroLimite == true)
        {
            RePosition = true;
        }

        if (RePosition == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, targ.transform.position, .30f);
        }

        if (transform.position == targ.transform.position)
        {
            DentroLimite = false;
            RePosition = false;
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && seguir == true)
        {
            pressed = true;

            if (valPos == false)
            {
                //positionObj = this.gameObject.transform;
                //Debug.Log(positionObj.position);
                valPos = true;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "animalEtapa2" && pressed == true)
        {
            seguir = false;
            col = collision.gameObject;
        }
        else if (collision.gameObject.name == "Limite")
        {
            DentroLimite = true;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "animalEtapa2" && pressed == true)
        {
            seguir = true;
        }

    }

    // Generar Oro
    IEnumerator generadorOro()
    {
        yield return new WaitForSeconds(2.5f);
        Moneda.GetComponent<Animator>().SetBool("active", false);
        yield return new WaitForSeconds(2.5f);
        GameObject.Find("ControladorEventos").GetComponent<EventControllerEvo1>().Oro += 5;
        Moneda.GetComponent<Animator>().SetBool("active", true);

        StartCoroutine(generadorOro());
    }

    IEnumerator ChangeAnimation()
    {
        VarChangeAnimation = Random.Range(0f, 5f);
        yield return new WaitForSeconds(VarChangeAnimation);
        GetComponent<Animator>().SetBool("ChangeIdle", true);
        yield return new WaitForSeconds(0.3f);
        GetComponent<Animator>().SetBool("ChangeIdle", false);
        StartCoroutine(ChangeAnimation());
    }

}