﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventControllerEvo1 : MonoBehaviour
{

    public GameObject Cerdogcatmono;
    public GameObject Motoperrdo;
    public GameObject Monaperrata;
    public GameObject Perranato;
    public GameObject Montorro;
    public GameObject Zerdonato;
    public GameObject Mocerrato;
    public GameObject Ramonerdo;
    public GameObject Pacermonato;
    public GameObject Monpezrrato;
    public GameObject Monarapez;
    public GameObject Monapezto;
    public GameObject Ratanamon;
    public GameObject Rapamonato;
    public GameObject Tomonparana;
    public GameObject Perronerdo;
    public GameObject Perronozgato;
    public GameObject Tomerrono;
    public GameObject Perronato;
    public GameObject Gaparronoto;
    public GameObject Pezerrono;
    public GameObject Percerrata;
    public GameObject Perraneirdo;
    public GameObject Pepanerdo;
    public GameObject Pezperronata;
    public GameObject Perranaropez;
    public GameObject Perronozto;
    public GameObject Ratorrona;
    public GameObject Ratatono;
    public GameObject Ranarrono;
    public GameObject Cergatopez;
    public GameObject Cermorryto;
    public GameObject Racertomo;
    public GameObject Gapatocermoto;
    public GameObject Pepecermono;
    public GameObject Petracerno;
    public GameObject Cermorrana;
    public GameObject Certorrono;
    public GameObject Cratamopez;
    public GameObject Racernapez;
    public GameObject Peztocerno;
    public GameObject Ceratanomo;
    public GameObject Cerapamono;
    public GameObject Cerranato;
    public GameObject Motoropez;
    public GameObject Gamonezdo;
    public GameObject Tomonerryz;
    public GameObject Morapeznato;
    public GameObject Mogapeztoto;
    public GameObject Perramota;
    public GameObject Ranopezra;
    public GameObject Torropapez;
    public GameObject Mocenorra;
    public GameObject Monerdopez;
    public GameObject Pamonpezdo;
    public GameObject Motarranez;
    public GameObject Ramotonez;
    public GameObject Ramoneto;
    public GameObject Morogcata;
    public GameObject Gamorerdota;
         
    private GameObject AnimalEvo2;

    private GameObject NameChange;

    //Variables de estadisticas 
    public float Oro = 0;

    public bool GenerarFusion = false;
    public GameObject prefab;
    public GameObject object2;
    public GameObject object3;

    public GameObject Target;

    public float posX = 0;
    public float posY = 0;


    // Start is called before the first frame update
    void Start()
    {}
    // Update is called once per frame
    void Update()
    {
        if (GenerarFusion == true) {
            posX = object2.transform.position.x + object3.transform.position.x;
            posY = object2.transform.position.y + object3.transform.position.y;

            object2.GetComponent<SpriteRenderer>().enabled = false;
            object3.GetComponent<SpriteRenderer>().enabled = false;

            object2.GetComponent<CircleCollider2D>().enabled = false;
            object3.GetComponent<CircleCollider2D>().enabled = false;

            Instantiate(prefab, new Vector3(posX/2, posY/2, 0), Quaternion.identity);
            StartCoroutine(remove(AnimalEvo2));

            GenerarFusion = false;
        }

        if (Input.GetMouseButtonDown(0)) {
            Target.transform.position = new Vector3(Random.Range(-4.36f, 4.5f), Random.Range(-7f, 4.63f), 0);
        }
    }


    public void ContraladorEvo0(string Animal_1, string Animal_2) {

        // Dogcat

        if (Animal_1 == "Dogcat" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Motoropez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Dogcat" && Animal_2 == "Morata")
        {
            AnimalEvo2 = Morogcata;
            GenerarFusion = true;
        }

        // Gaterdo

        if (Animal_1 == "Gaterdo" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Perronerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gaterdo" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Gamonezdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gaterdo" && Animal_2 == "Morata")
        {
            AnimalEvo2 = Gamorerdota;
            GenerarFusion = true;
        }

        // Pegato

        if (Animal_1 == "Pegato" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Perronozgato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pegato" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Cergatopez;
            GenerarFusion = true;
        }

        // Tomerry

        if (Animal_1 == "Tomerry" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Tomerrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Tomerry" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Cermorryto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Tomerry" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Tomonerryz;
            GenerarFusion = true;
        }

        // Ranato

        if (Animal_1 == "Ranato" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Perronato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranato" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Racertomo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranato" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Morapeznato;
            GenerarFusion = true;
        }

        // Gapatoto

        if (Animal_1 == "Gapatoto" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Gaparronoto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gapatoto" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Gapatocermoto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gapatoto" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Mogapeztoto;
            GenerarFusion = true;
        }

        // Perdo

        if (Animal_1 == "Perdo" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Cerdogcatmono;
            GenerarFusion = true;
        }

        // Pezro

        if (Animal_1 == "Pezro" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Motoperrdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pezro" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Pepecermono;
            GenerarFusion = true;
        }

        // Perrata

        if (Animal_1 == "Perrata" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Monaperrata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrata" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Petracerno;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrata" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Perramota;
            GenerarFusion = true;
        }

        // Raperro

        if (Animal_1 == "Raperro" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Perranato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Raperro" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Cermorrana;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Raperro" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Ranopezra;
            GenerarFusion = true;
        }

        // Patorro

        if (Animal_1 == "Patorro" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Montorro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Patorro" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Certorrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Patorro" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Torropapez;
            GenerarFusion = true;
        }

        // Pecerdo

        if (Animal_1 == "Patorro" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Zerdonato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Patorro" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Pezerrono;
            GenerarFusion = true;
        }

        // Cerrata

        if (Animal_1 == "Cerrata" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Mocerrato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerrata" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Percerrata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerrata" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Mocenorra;
            GenerarFusion = true;
        }

        // Ranerdo

        if (Animal_1 == "Ranerdo" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Ramonerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranerdo" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Perraneirdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranerdo" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Monerdopez;
            GenerarFusion = true;
        }

        // Cerpato

        if (Animal_1 == "Cerpato" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Pacermonato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerpato" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Pepanerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerpato" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Pamonpezdo;
            GenerarFusion = true;
        }

        // Ratapez

        if (Animal_1 == "Ratapez" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Monpezrrato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratapez" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Pezperronata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratapez" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Cratamopez;
            GenerarFusion = true;
        }

        // Ranapez

        if (Animal_1 == "Ranapez" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Monarapez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranapez" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Perranaropez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranapez" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Racernapez;
            GenerarFusion = true;
        }

        // Patez

        if (Animal_1 == "Patez" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Monapezto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Patez" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Perronozto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Patez" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Peztocerno;
            GenerarFusion = true;
        }

        // Ratana

        if (Animal_1 == "Ratana" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Ratanamon;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratana" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Ratorrona;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratana" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Ceratanomo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratana" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Motarranez;
            GenerarFusion = true;
        }

        // Ratato

        if (Animal_1 == "Ratato" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Rapamonato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratato" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Ratatono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratato" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Cerapamono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ratato" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Ramotonez;
            GenerarFusion = true;
        }

        // Ranapato

        if (Animal_1 == "Ranapato" && Animal_2 == "Monato")
        {
            AnimalEvo2 = Tomonparana;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranapato" && Animal_2 == "Perrono")
        {
            AnimalEvo2 = Ranarrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranapato" && Animal_2 == "Cermono")
        {
            AnimalEvo2 = Cerranato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Ranapato" && Animal_2 == "Monopez")
        {
            AnimalEvo2 = Ramoneto;
            GenerarFusion = true;
        }

        // Monato

        if (Animal_1 == "Monato" && Animal_2 == "Perdo")
        {
            AnimalEvo2 = Cerdogcatmono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Pezro")
        {
            AnimalEvo2 = Motoperrdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Perrata")
        {
            AnimalEvo2 = Monaperrata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Raperro")
        {
            AnimalEvo2 = Perranato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Patorro")
        {
            AnimalEvo2 = Montorro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Pecerdo")
        {
            AnimalEvo2 = Zerdonato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Cerrata")
        {
            AnimalEvo2 = Mocerrato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Ranerdo")
        {
            AnimalEvo2 = Ramonerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Cerpato")
        {
            AnimalEvo2 = Pacermonato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Ratapez")
        {
            AnimalEvo2 = Monpezrrato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Ranapez")
        {
            AnimalEvo2 = Monarapez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Patez")
        {
            AnimalEvo2 = Monapezto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Ratana")
        {
            AnimalEvo2 = Ratanamon;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Ratato")
        {
            AnimalEvo2 = Rapamonato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monato" && Animal_2 == "Ranapato")
        {
            AnimalEvo2 = Tomonparana;
            GenerarFusion = true;
        }

        // Perrono

        if (Animal_1 == "Perrono" && Animal_2 == "Gaterdo")
        {
            AnimalEvo2 = Perronerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Pegato")
        {
            AnimalEvo2 = Perronozgato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Morata")
        {
            AnimalEvo2 = Tomerrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Ranato")
        {
            AnimalEvo2 = Perronato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Gapatoto")
        {
            AnimalEvo2 = Gaparronoto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Pecerdo")
        {
            AnimalEvo2 = Pezerrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Cerrata")
        {
            AnimalEvo2 = Percerrata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Ranerdo")
        {
            AnimalEvo2 = Perraneirdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Cerpato")
        {
            AnimalEvo2 = Pepanerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Ratapez")
        {
            AnimalEvo2 = Pezperronata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Ranapez")
        {
            AnimalEvo2 = Perranaropez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Patez")
        {
            AnimalEvo2 = Perronozto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Ratana")
        {
            AnimalEvo2 = Ratorrona;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Ratato")
        {
            AnimalEvo2 = Ratatono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perrono" && Animal_2 == "Ranapato")
        {
            AnimalEvo2 = Ranarrono;
            GenerarFusion = true;
        }

        // Cermono

        else if (Animal_1 == "Cermono" && Animal_2 == "Pegato")
        {
            AnimalEvo2 = Cergatopez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Morata")
        {
            AnimalEvo2 = Cermorryto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Ranato")
        {
            AnimalEvo2 = Racertomo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Gapatoto")
        {
            AnimalEvo2 = Gapatocermoto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Pezro")
        {
            AnimalEvo2 = Pepecermono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Perrata")
        {
            AnimalEvo2 = Petracerno;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Raperro")
        {
            AnimalEvo2 = Cermorrana;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Patorro")
        {
            AnimalEvo2 = Certorrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Ratapez")
        {
            AnimalEvo2 = Cratamopez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Ranapez")
        {
            AnimalEvo2 = Racernapez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Patez")
        {
            AnimalEvo2 = Peztocerno;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Ratana")
        {
            AnimalEvo2 = Ceratanomo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Ratato")
        {
            AnimalEvo2 = Cerapamono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cermono" && Animal_2 == "Ranapato")
        {
            AnimalEvo2 = Cerranato;
            GenerarFusion = true;
        }

        // Monopez

        else if (Animal_1 == "Monopez" && Animal_2 == "Dogcat")
        {
            AnimalEvo2 = Motoropez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Gaterdo")
        {
            AnimalEvo2 = Gamonezdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Tomerry")
        {
            AnimalEvo2 = Tomonerryz;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Ranato")
        {
            AnimalEvo2 = Morapeznato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Gapatoto")
        {
            AnimalEvo2 = Mogapeztoto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Perrata")
        {
            AnimalEvo2 = Perramota;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Raperro")
        {
            AnimalEvo2 = Ranopezra;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Patorro")
        {
            AnimalEvo2 = Torropapez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Cerrata")
        {
            AnimalEvo2 = Mocenorra;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Ranerdo")
        {
            AnimalEvo2 = Monerdopez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Cerpato")
        {
            AnimalEvo2 = Pamonpezdo;
            GenerarFusion = true;
        }
        
        else if (Animal_1 == "Monopez" && Animal_2 == "Ratana")
        {
            AnimalEvo2 = Motarranez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Ratato")
        {
            AnimalEvo2 = Ramotonez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Ranapato")
        {
            AnimalEvo2 = Ramoneto;
            GenerarFusion = true;
        }

        // Morata

        else if (Animal_1 == "Monopez" && Animal_2 == "Dogcat")
        {
            AnimalEvo2 = Morogcata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Monopez" && Animal_2 == "Gaterdo")
        {
            AnimalEvo2 = Gamorerdota;
            GenerarFusion = true;
        }
    }

    IEnumerator remove(GameObject AnimalEvo)
    {
        Destroy(object2);
        Destroy(object3);
        Vector3 reload = new Vector3(posX / 2, posY / 2, this.transform.position.z);
        yield return new WaitForSeconds(1.6f);
        NameChange = Instantiate(AnimalEvo, reload, Quaternion.identity) as GameObject;
        NameChange.name = NameChange.name.Replace("(Clone)", "");
    }


}
