﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventControllerEvo0 : MonoBehaviour
{
    // Prefab Evo1

    public GameObject Cerpato;
    public GameObject Cermono;
    public GameObject Cerrata;
    public GameObject Gaterdo;
    public GameObject Pecerdo;
    public GameObject Perdo;
    public GameObject Ranerdo;

    public GameObject Morata;
    public GameObject Perrata;
    public GameObject Ratana;
    public GameObject Ratapez;
    public GameObject Ratato;
    public GameObject Tomerry;

    public GameObject Pegato;
    public GameObject Monopez;
    public GameObject Patez;
    public GameObject Pezro;
    public GameObject Ranapez;

    public GameObject Dogcat;
    public GameObject Patorro;
    public GameObject Raperro;
    public GameObject Perrono;

    public GameObject Ranapato;
    public GameObject Gapatoto;
    public GameObject Monpato;

    public GameObject Monato;
    public GameObject Ranato;

    public GameObject Ramono;

    private GameObject AnimalEvo1;

    private GameObject NameChange;

    //Variables de estadisticas 
    public float Oro = 0;

    public bool GenerarFusion = false;
    public GameObject prefab;
    public GameObject object2;
    public GameObject object3;

    public GameObject Target;

    public float posX = 0;
    public float posY = 0;


    // Start is called before the first frame update
    void Start()
    { }
    // Update is called once per frame
    void Update()
    {
        if (GenerarFusion == true)
        {
            posX = object2.transform.position.x + object3.transform.position.x;
            posY = object2.transform.position.y + object3.transform.position.y;

            object2.GetComponent<SpriteRenderer>().enabled = false;
            object3.GetComponent<SpriteRenderer>().enabled = false;

            object2.GetComponent<CircleCollider2D>().enabled = false;
            object3.GetComponent<CircleCollider2D>().enabled = false;

            Instantiate(prefab, new Vector3(posX / 2, posY / 2, 0), Quaternion.identity);

            StartCoroutine(remove(AnimalEvo1));

            GenerarFusion = false;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Target.transform.position = new Vector3(Random.Range(-4.36f, 4.5f), Random.Range(-7f, 4.63f), 0);
        }
    }


    public void ContraladorEvo0(string Animal_1, string Animal_2)
    {

        // Cerdo

        if (Animal_1 == "Cerdo" && Animal_2 == "Pato")
        {
            AnimalEvo1 = Cerpato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerdo" && Animal_2 == "Mono")
        {
            AnimalEvo1 = Cermono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerdo" && Animal_2 == "Gato")
        {
            AnimalEvo1 = Gaterdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerdo" && Animal_2 == "Rata")
        {
            AnimalEvo1 = Cerrata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerdo" && Animal_2 == "Perro")
        {
            AnimalEvo1 = Perdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerdo" && Animal_2 == "Pez")
        {
            AnimalEvo1 = Pecerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Cerdo" && Animal_2 == "Rana")
        {
            AnimalEvo1 = Ranerdo;
            GenerarFusion = true;
        }

        //Rata

        if (Animal_1 == "Rata" && Animal_2 == "Pato")
        {
            AnimalEvo1 = Ratato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rata" && Animal_2 == "Gato")
        {
            AnimalEvo1 = Tomerry;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rata" && Animal_2 == "Perro")
        {
            AnimalEvo1 = Perrata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rata" && Animal_2 == "Pez")
        {
            AnimalEvo1 = Ratapez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rata" && Animal_2 == "Mono")
        {
            AnimalEvo1 = Morata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rata" && Animal_2 == "Cerdo")
        {
            AnimalEvo1 = Cerrata;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rata" && Animal_2 == "Rana")
        {
            AnimalEvo1 = Ratana;
            GenerarFusion = true;
        }

        //Pez

        if (Animal_1 == "Pez" && Animal_2 == "Pato")
        {
            AnimalEvo1 = Patez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pez" && Animal_2 == "Rana")
        {
            AnimalEvo1 = Ranapez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pez" && Animal_2 == "Perro")
        {
            AnimalEvo1 = Pezro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pez" && Animal_2 == "Gato")
        {
            AnimalEvo1 = Pegato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pez" && Animal_2 == "Mono")
        {
            AnimalEvo1 = Monopez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pez" && Animal_2 == "Cerdo")
        {
            AnimalEvo1 = Pecerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pez" && Animal_2 == "Rata")
        {
            AnimalEvo1 = Ratapez;
            GenerarFusion = true;
        }

        //Perro

        if (Animal_1 == "Perro" && Animal_2 == "Pato")
        {
            AnimalEvo1 = Patorro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perro" && Animal_2 == "Rana")
        {
            AnimalEvo1 = Raperro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perro" && Animal_2 == "Pez")
        {
            AnimalEvo1 = Pezro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perro" && Animal_2 == "Gato")
        {
            AnimalEvo1 = Dogcat;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perro" && Animal_2 == "Mono")
        {
            AnimalEvo1 = Perrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perro" && Animal_2 == "Cerdo")
        {
            AnimalEvo1 = Perdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Perro" && Animal_2 == "Rata")
        {
            AnimalEvo1 = Perrata;
            GenerarFusion = true;
        }

        //Pato

        if (Animal_1 == "Pato" && Animal_2 == "Mono")
        {
            AnimalEvo1 = Monpato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pato" && Animal_2 == "Rana")
        {
            AnimalEvo1 = Ranapato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pato" && Animal_2 == "Gato")
        {
            AnimalEvo1 = Gapatoto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pato" && Animal_2 == "Perro")
        {
            AnimalEvo1 = Patorro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pato" && Animal_2 == "Pez")
        {
            AnimalEvo1 = Patez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pato" && Animal_2 == "Cerdo")
        {
            AnimalEvo1 = Cerpato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Pato" && Animal_2 == "Rata")
        {
            AnimalEvo1 = Ratato;
            GenerarFusion = true;
        }

        //Gato

        if (Animal_1 == "Gato" && Animal_2 == "Pato")
        {
            AnimalEvo1 = Gapatoto;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gato" && Animal_2 == "Rana")
        {
            AnimalEvo1 = Ranato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gato" && Animal_2 == "Perro")
        {
            AnimalEvo1 = Dogcat;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gato" && Animal_2 == "Pez")
        {
            AnimalEvo1 = Pegato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gato" && Animal_2 == "Mono")
        {
            AnimalEvo1 = Monato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gato" && Animal_2 == "Cerdo")
        {
            AnimalEvo1 = Gaterdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Gato" && Animal_2 == "Rata")
        {
            AnimalEvo1 = Tomerry;
            GenerarFusion = true;
        }

        //Rana

        if (Animal_1 == "Rana" && Animal_2 == "Pato")
        {
            AnimalEvo1 = Ranapato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rana" && Animal_2 == "Gato")
        {
            AnimalEvo1 = Ranato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rana" && Animal_2 == "Perro")
        {
            AnimalEvo1 = Raperro;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rana" && Animal_2 == "Pez")
        {
            AnimalEvo1 = Ranapez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rana" && Animal_2 == "Mono")
        {
            AnimalEvo1 = Ramono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rana" && Animal_2 == "Cerdo")
        {
            AnimalEvo1 = Ranerdo;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Rana" && Animal_2 == "Rata")
        {
            AnimalEvo1 = Ratana;
            GenerarFusion = true;
        }

        //Mono

        if (Animal_1 == "Mono" && Animal_2 == "Pato")
        {
            AnimalEvo1 = Monpato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Mono" && Animal_2 == "Gato")
        {
            AnimalEvo1 = Monato;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Mono" && Animal_2 == "Perro")
        {
            AnimalEvo1 = Perrono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Mono" && Animal_2 == "Pez")
        {
            AnimalEvo1 = Monopez;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Mono" && Animal_2 == "Rana")
        {
            AnimalEvo1 = Ramono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Mono" && Animal_2 == "Cerdo")
        {
            AnimalEvo1 = Cermono;
            GenerarFusion = true;
        }
        else if (Animal_1 == "Mono" && Animal_2 == "Rata")
        {
            AnimalEvo1 = Morata;
            GenerarFusion = true;
        }

    }

    IEnumerator remove(GameObject AnimalEvo)
    {
        Destroy(object2);
        Destroy(object3);
        Vector3 reload = new Vector3(posX / 2, posY / 2, this.transform.position.z);
        yield return new WaitForSeconds(1.6f);
        NameChange = Instantiate(AnimalEvo, reload, Quaternion.identity) as GameObject;
        NameChange.name = NameChange.name.Replace("(Clone)", "");
    }


}
