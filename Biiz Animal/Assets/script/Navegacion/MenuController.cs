﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public GameObject Panel;

    // objetos de validacion de sonido y musica
    public GameObject Xsonido;
    public GameObject Xmusica;

    // Validador de botones
    public bool validador = false;

    public void MainBtn() {
        if (validador == false) {
            validador = true;
            StartCoroutine(TiempoValidador());
            if (Panel.GetComponent<Animator>().GetBool("In") == true) {
                Panel.GetComponent<Animator>().SetBool("In", false);
                StartCoroutine(FinMainBtn());
            }
            else {
                Panel.SetActive(true);
                Panel.GetComponent<Animator>().SetBool("In", true);
            }
        }
    }

    IEnumerator FinMainBtn() {
        yield return new WaitForSeconds(0.8f);
        Panel.SetActive(false);
    }

    IEnumerator TiempoValidador(){
        yield return new WaitForSecondsRealtime(0.8f);
        validador = false;
    }

    // Navegacion de opciones

    public void InOutSonido() {
        if (Xsonido.activeSelf == true)
        {
            Xsonido.SetActive(false);
        }
        else {
            Xsonido.SetActive(true);
        }
    }

    public void InOutMusica()
    {
        if (Xmusica.activeSelf == true)
        {
            Xmusica.SetActive(false);
        }
        else
        {
            Xmusica.SetActive(true);
        }
    }

}
